*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

JS (ES5), jQuery, Bootstrap 3, Huebee (JS color picker lib, had to be slightly hacked to suit the needs of this app),
PHP, SQLite

### Introduction ###

This a color survery. Each user inputs their age and gender and then they are shown a series of images. Each image disappears after
ten seconds and the user is asked to recreate the image using at least three blobs of color (color is important, not shape). The 
user gets an instant feedback on how well they scored in each image.

![demo](readme/recording.gif)

All data is collected (in an SQLite DB) and can be viewed in a simple CSV format.

![demo data](readme/data.png)

### Deployment ###

Works out of the box on XAMPP or a typical web hosting (Apache, PHP, PHP extension for SQLite).