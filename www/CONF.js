var CONF = {
    circle: {
        minCount: 3,
        maxCount: 5,
        radius: 40,                 //actual pixels! (look different size on different resolution canvases!)
        highlightSelected: {
            color: '#a3ccf5',       //'#b3cce6', //'#3399ff',
            offset: 8,
            lineWidth: 2
        },
        initialColor: '#FFF'       //CARE must be from current colorpicker and must be hex (white is a good choice)
    },
    colorPickerSettings: {
        hue0: 0,                    //what hue the palette starts with (now red)
        hues: 23,
        shades: 5,
        saturations: 11
    },
    score: {
        //TODO how is calculated
        //anomalies:
        //  - white and black have similaity 0.36 (same hue, same saturation, different shade)
        //  - almost impossible to reach socre below 50% => quite nice actually
        alpha: 1.3,
        MBRhelpFactor: 1.0,
        overallScoreHelper: 1.0,
        stars: 5                    //max score in star
    },
    countdownDuration: 10,          //s
    owlShutUp: 4000                 //ms //the smart owl will shut up after 4s
};






if (DEBUG_MODE){
    CONF.circle.minCount = 1;
    CONF.countdownDuration = 2;
}