const MouseButton = { //a simple enum //TODO solitary file
    LEFT: 1,
    MIDDLE: 2,
    RIGHT: 3
};
const Keys = {
    ENTER: 13,
    ESC: 27
};


//TODO colors are set incorrectly


//class
function CanvasAnimator(canvasController, guiController){
    this.guiController = guiController; //TODO remove out if possible
    this.canvasController = canvasController;
    this.colorPicker = new ColorPicker($('#colorpickerAnchor')[0], canvasController);

    this.canvasJElement = $('#gameCanvas');
    this.offsetFromCenterOfCircleLastClicked = new Position(0,0);
    this.indexOfDraggedCircle = null;
    this.dragLength = 0;
    this.lastPositionInDrag = null;

    //left click - new circle
    //left click existing - open color picker on it
    //left click existing with color picker on it - close color picker
    //drag - (also drag color picker if open)
    //right click - dont show context menu
    //right click existing - delete



    if (CanvasAnimator._used){
        throw new Error('singleton abused');
    } else {
        CanvasAnimator._used = true;
    }
}





// public methods ******************************************************
CanvasAnimator.prototype.enable = function(){
    //bind event handlers for mouse
    this.canvasJElement.click(this._getClickHandler());

    this.canvasJElement.mousedown(this._getMousedownHandler());
    this.canvasJElement.mousemove(this._getMousemoveHandler());
    this.canvasJElement.mouseup(this._getMouseupHandler());
    this.canvasJElement.contextmenu(this._getRightClickHandler());
    //context menu is a better version of event for right click
    this.canvasController.timeEnabled = new Date().getTime();

    $('body').mouseup(this._getOverBodyDragHandler());
    $('body').click(this._getOverBodyClickHandler());
    //makes sure dragging and circle highlighting works correctly when mouse leaves canvas
    //hides color picker when clicked else where

    //$(document).keydown(this._getKeyboardHandler());
    //removed due to buggy behavior - probably connected somehow with focus
};
CanvasAnimator.prototype.disable = function(){
    var self=this;
    ['click', 'mousedown', 'mousemove', 'mouseup', 'contextmenu'].forEach(function(eventString){
        self.canvasJElement.off(eventString);
    });
    ['mouseup', 'click'].forEach(function(eventString){
        $('body').off(eventString);
    });
    //$(document).off('keydown');
    //CARE these two above could potentially remove other handlers TODO differently
    this.canvasController.highlightSelected = false; //TODO merge with redraw
    this.canvasController.redraw();
};




















// privates *************************************************************
CanvasAnimator.prototype._getClickHandler = function(){
    var self=this;
    return function(event){
        if (event.which===MouseButton.LEFT){
            self._handleClickLeft(event);
        }
    };
};
CanvasAnimator.prototype._getMousedownHandler = function(){
    var self=this;
    return function(event){
        if (event.which===MouseButton.LEFT){
            self._handleMousedownLeft(event);
        }
    };
};
CanvasAnimator.prototype._getMousemoveHandler = function(){
    var self=this;
    return function(event){
        if (self.indexOfDraggedCircle == null){ return; }

        var cssPos = self.canvasController.getCursorPositionInCanvasCSS(event);
        if (self.colorPicker.isOpen()){
            self.colorPicker.move(cssPos, event);
        }
        var pos = self._getCorrectedPositionInCanvasData(event);
        var draggedCircle = self.canvasController.circles[self.indexOfDraggedCircle]; //TODO maybe into canvas controller
        draggedCircle.setPosition(pos.x, pos.y, self.canvasController._getTimeElapsed()); //TODO maybe merge with redraw //TODO split
        self.canvasController.redraw();
        self.dragLength += self._getLastPartialDragLength(pos);
        self.lastPositionInDrag = pos;
    };
};
CanvasAnimator.prototype._getMouseupHandler = function(){
    var self=this;
    return function(event){
        if (event.which===MouseButton.LEFT){
            self._handleMouseupLeft(event);
        }
    };
};
CanvasAnimator.prototype._getRightClickHandler = function(){
    var self=this;
    return function(event){
        var pos = self.canvasController.getCursorPositionInCanvasData(event);
        self.canvasController.removeTopmostCircleAt(pos);
        GuiController.preventDefaultEventHandling(event);
        self.colorPicker.close();
        //prevents little popup
        return false; //TODO this necessary?
    };
};
CanvasAnimator.prototype._getOverBodyDragHandler = function(){
    var self=this;
    return function(event){
        if (!self._isTargetCanvasOrColorPicker(event)) {
            self._handleMouseupLeft(event);
        }
    };
};
CanvasAnimator.prototype._getOverBodyClickHandler = function(){
    var self=this;
    return function(event){
        if (!self._isTargetCanvasOrColorPicker(event)){
            self.colorPicker.close();
        }
    };
};
CanvasAnimator.prototype._getKeyboardHandler = function(event){
    var self=this;
    return function(event){
        if (event.keyCode === Keys.ENTER || event.keyCode === Keys.ESC) {
            self.colorPicker.close();
        }
    };
};






CanvasAnimator.prototype._isTargetCanvasOrColorPicker = function(event){
    return $.contains($('#gameCanvasContainer')[0], event.target);
    //CARE [0] is important here
};
/**
 *
 * @param position Position()
 * @private
 */
CanvasAnimator.prototype._getLastPartialDragLength = function(position){
    var a = this.lastPositionInDrag;
    var b = position;
    var dx = Math.abs(a.x - b.x);
    var dy = Math.abs(a.y - b.y);
    return Math.sqrt(dx*dx + dy*dy);
};
CanvasAnimator.prototype._handleClickLeft = function(event){
    const MIN_DRAG_LENGTH = 50;
    if (this.dragLength > MIN_DRAG_LENGTH){ return; }
    //bear in mind, that drag and release is also a click

    var pos = this.canvasController.getCursorPositionInCanvasData(event);
    var idx = this.canvasController.getTopmostClickedCircleIdx(pos); //TODO redundant, can be taken from drag tracker
    var cssPos = this.canvasController.getCursorPositionInCanvasCSS(event);
    if (idx == null) {
        this.colorPicker.close();
        const MAX_CIRCLES = CONF.circle.maxCount;
        if (this.canvasController.circles.length < MAX_CIRCLES){
            this.canvasController.createCircle(pos.x, pos.y);
            this.colorPicker.open(cssPos, event);
        } else {
            this.guiController.message('Sorry, you cannot use more than ' + MAX_CIRCLES + ' circles.');
        }
    } else {
        if(this.colorPicker.isOpen() && this.canvasController.wasSameCircleFocusedTwice()){
            this.colorPicker.close();
        } else {
            this.colorPicker.open(cssPos, event);
            var color = this.canvasController.getColor();
            this.colorPicker.setColor(color);
        }
    }
    //console.log('left click - topmost circle',idx, pos);
};
CanvasAnimator.prototype._handleMousedownLeft = function(event){
    var pos = this.canvasController.getCursorPositionInCanvasData(event);
    var idx = this.canvasController.getTopmostClickedCircleIdx(pos);
    if (idx != null){
        this.offsetFromCenterOfCircleLastClicked = this._getOffsetFromCenterOfCircle(pos, idx);
        this.indexOfDraggedCircle = idx;
        this.canvasController.focusCircle(idx);
        if (this.colorPicker.isOpen()){
            var color = this.canvasController.getColor();
            this.colorPicker.setColor(color);
        }
    }
    this.dragLength = 0;
    this.lastPositionInDrag = pos;
};
CanvasAnimator.prototype._handleMouseupLeft = function(event){
    this.indexOfDraggedCircle = null;
};


/**
 * @param event
 * @returns {Position}
 * @private
 */
CanvasAnimator.prototype._getCorrectedPositionInCanvasData = function(event){ //TODO rename //TODO into canvas contorller?
    var pos = this.canvasController.getCursorPositionInCanvasData(event);

    var canvas = this.canvasController.getDOMcanvas();
    const FORBIDDEN_ZONE = 5;   //5px around all edges (for smooth user experience)
    const minX = FORBIDDEN_ZONE;
    const maxX = canvas.width - FORBIDDEN_ZONE;
    const minY = FORBIDDEN_ZONE;
    const maxY = canvas.height - FORBIDDEN_ZONE;

    //correct x,y so that circle doesn't go over the edge
    var posX = pos.x - this.offsetFromCenterOfCircleLastClicked.x;
    posX = (posX < minX) ? minX : ((posX > maxX) ? maxX : posX);
    var posY = pos.y - this.offsetFromCenterOfCircleLastClicked.y;
    posY = (posY < minY) ? minY : ((posY > maxY) ? maxY : posY);
    return new Position(posX, posY);
};
/**
 *
 * @param position Position()
 * @param index
 * @private
 */
CanvasAnimator.prototype._getOffsetFromCenterOfCircle = function(position, index){ //TODO into canvas controller?
    return new Position(
        position.x - this.canvasController.circles[index].x,
        position.y - this.canvasController.circles[index].y
    );
};



CanvasAnimator._used = false;