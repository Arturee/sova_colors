//class
function CanvasController(canvasjQueryElement){
    this.canvas = canvasjQueryElement;
    //TODO remove with care
    //TODO we (only) need graphic context and access to MBR (width, height)

    this.circles = [];
    this.focusedCircleIdx = null;
    this._previousIdx = null;
    this.lastCircleColor = CONF.circle.initialColor;
    this.backgroudPattern = null;
    this.highlightSelected = false;
    this.timeEnabled = new Date().getTime(); //ms

    this._generateBackground();



    if (CanvasController._used){
        throw new Error('singleton abused');
    } else {
        CanvasController._used = true;
    }

    //TODO automatic redraw everywhere or nowhere?
}





//public methods ************************************************************************
/**
 *
 * @param position Position()
 * @returns {*} null if none clicked
 * @private
 */
CanvasController.prototype.getTopmostClickedCircleIdx = function(position){
    var highestIndexYet = -1;
    for (var i = 0; i < this.circles.length; i++) {
        if (this.circles[i].hitTest(position.x, position.y)) {
            if (i > highestIndexYet) {
                highestIndexYet = i;
                //circles sorted by z-axis (lower index => further in the back)
            }
        }
    }
    return highestIndexYet > -1 ? highestIndexYet : null;
};
/**
 *
 * @param position Position()
 */
CanvasController.prototype.removeTopmostCircleAt = function(position){
    var idx = this.getTopmostClickedCircleIdx(position);
    if (idx != null) {
        this.removeCircle(idx);
    }
    this.highlightSelected = false; //TODO rename to highlight focused
    this.redraw();  //TODO take out?
};

CanvasController.prototype.getDOMcanvas = function(){ //TODO maybe only have DOM canvas!
    return this.canvas[0];
    //[0] converts jQuery object to a DOM-object
    //must be done in order to access many useful properties
};
/**
 *
 * @param x pixel position in canvas (not css position)
 * @param y pixel position in canvas (not css position)
 */
CanvasController.prototype.createCircle = function(x, y){
    this.circles.push(new Circle(x, y, this.lastCircleColor, this._getTimeElapsed()));
    var idx = this.circles.length-1;
    this.focusCircle(idx); //also redraws
    //console.log('circle created');
};
/**
 * @param event
 * @returns {Position}
 */
CanvasController.prototype.getCursorPositionInCanvasCSS = function(event) { //TODO maybe move out
    var rect = this.getDOMcanvas().getBoundingClientRect();
    var x = (event.clientX - rect.left);
    var y = (event.clientY - rect.top);
    return new Position(x,y);
};
/**
 * @param event
 * @returns {Position}
 */
CanvasController.prototype.getCursorPositionInCanvasData = function(event) {
    var cssPos = this.getCursorPositionInCanvasCSS(event);
    var DOMcanvas = this.getDOMcanvas();
    var rect = DOMcanvas.getBoundingClientRect(); //rect is from css
    var x = cssPos.x * (DOMcanvas.width/rect.width); //TODO get real size() class?
    var y = cssPos.y * (DOMcanvas.height/rect.height);
    return new Position(x,y);
};
CanvasController.prototype.changeColorOfFocusedCircle = function(colorHex){
    var idx = this.focusedCircleIdx;
    if (idx!=null){
        var focusedCircle = this.circles[idx];
        focusedCircle.setColor(colorHex, this._getTimeElapsed());
        this.lastCircleColor = colorHex;
        this.redraw();
    }
};
CanvasController.prototype.redraw = function(){
    if (this.backgroudPattern===null) return;

    var context = this._getContext();
    var canvas = this.getDOMcanvas();
    //redraw background of canvas
    context.fillStyle = this.backgroudPattern;
    context.fillRect(0, 0, canvas.width, canvas.height);
    //redraw all circles
    var circles = this.circles;
    for (var i=0; i < circles.length; i++) {
        this._drawCircle(circles[i].x, circles[i].y, circles[i].colorHex, Circle.getRadius());
    }
    if (this.highlightSelected && this.focusedCircleIdx != null){
        var selectedCricle = circles[this.focusedCircleIdx];
        this.highlightCircle(
            selectedCricle.x,
            selectedCricle.y,
            Circle.getRadius() + CONF.circle.highlightSelected.offset,
            CONF.circle.highlightSelected.color,
            CONF.circle.highlightSelected.lineWidth);
    }

    //CARE here don't try to use context.save/restore() it is (probably) bugged :(
};
CanvasController.prototype.reset = function(){
    //remove all circles
    this.circles = [];
    this._setFocusedCircleIdx(null);
    this.redraw();
};
CanvasController.prototype.removeCircle = function(idx){
    this.circles.splice(idx,1);
    this._setFocusedCircleIdx(null);
    //this.redraw();
};
/**
 * @param pixelSize Size()
 */
CanvasController.prototype.setSize = function(pixelSize){
    var actualCanvas = this._getContext().canvas;
    actualCanvas.width = pixelSize.width;
    actualCanvas.height = pixelSize.height;
};
CanvasController.prototype.highlightCircle = function(x,y,r,color,lineWidth,dashSpacing){
    var context = this._getContext();
    context.beginPath();
    context.arc(x, y, r, 0, 2 * Math.PI, false);
    context.strokeStyle = color;
    context.lineWidth = lineWidth;
    if (dashSpacing != null){
        context.setLineDash(dashSpacing);
    }
    context.stroke();
};
CanvasController.prototype.focusCircle = function(idx){
    this._setFocusedCircleIdx(idx);
    this.highlightSelected = true;
    this.lastCircleColor = this.circles[idx].colorHex;
    this.redraw();
};
CanvasController.prototype.getColor = function(){
    return this.lastCircleColor;
};
CanvasController.prototype.wasSameCircleFocusedTwice = function(idx){
    return this._previousIdx === this.focusedCircleIdx;
};







//private methods *****************************************************************
CanvasController.prototype._getTimeElapsed = function(){
    return new Date().getTime() - this.timeEnabled;
};
CanvasController.prototype._getContext = function(){
    return this.getDOMcanvas().getContext('2d');
};
CanvasController.prototype._drawCircle = function(x, y, colorHex, radius){
    var context = this._getContext();
    context.beginPath();
    context.arc(x, y, radius, 0, 2 * Math.PI, false);
    context.fillStyle = colorHex;
    context.fill();
    //console.log('drawing', x, y, colorHex, radius);
};
CanvasController.prototype._setFocusedCircleIdx = function(idx){
    if (idx == null){
        this._previousIdx = null;
        this.focusedCircleIdx = null;
    } else {
        this._previousIdx = this.focusedCircleIdx;
        this.focusedCircleIdx = idx;
    }
};




CanvasController.prototype._generateBackground = function(){ //TODO look at
    const PATH_GRID_PATTERN_IMG ='imgs/fourgrid.png';
    var background = new Image();
    background.src=PATH_GRID_PATTERN_IMG;
    var self=this;
    background.onload = function(){
        var context = self._getContext();
        self.backgroudPattern = context.createPattern(background, 'repeat');
        //pattern will be stored in browser cache after it is generated
        self.redraw();
    };
};




CanvasController._used = false;