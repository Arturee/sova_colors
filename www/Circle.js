//class
function Circle(x, y, colorHex, ms){ //TODO consider class Graphix?
    this.x=x;
    this.y=y;
    this.colorHex=colorHex;
    this.createdTime = ms;
    this.editedTime = ms;
}



//public  *********************************************************
Circle.prototype.hitTest = function(x2,y2) {
    var dx = x2 - this.x;
    var dy = y2 - this.y;
    var r = Circle.getRadius();
    return (dx*dx + dy*dy < r*r); //equation of a circle
};
Circle.prototype.setColor = function(hex, elapsedTime) {
    this.colorHex = hex;
    this.editedTime = elapsedTime;
};
Circle.prototype.setPosition = function(x, y, elapsedTime) {
    this.x=x;
    this.y=y;
    this.editedTime = elapsedTime;
};
/**
 * @returns Color()
 */
Circle.prototype.getColor = function() {
    return Color.fromHex(this.colorHex);
};
Circle.prototype.toString = function(){
    return '['
        +  Math.round(this.x) + ","
        + Math.round(this.y) + ","
        + Color.fromHex(this.colorHex).toHexString() + ","
        + this.createdTime + ","
        + this.editedTime + ']';
};
Circle.getRadius = function(){
    return CONF.circle.radius; //px
};
Circle.getBoostedRadius = function(){
    return Circle.getRadius() * CONF.score.MBRhelpFactor; //px
};
/**
 * //TODO maybe move out
 *
 * @param graphicContext
 * @param canvasPixelWidth
 * @param canvasPixelHeight
 * @returns {ImageData}
 */
Circle.prototype.getMBRimageData = function (graphicContext, canvasPixelWidth, canvasPixelHeight){
    var r = Circle.getBoostedRadius();
    var width = 2 * r;
    var minX = this.x - r;
    var minY = this.y - r;
    var maxX = minX + width;
    var maxY = minY + width;
    minX = Math.max(0, minX);
    minY = Math.max(0, minY);
    maxX = Math.min(maxX, canvasPixelWidth);
    maxY = Math.min(maxY, canvasPixelHeight);
    //console.log(minX, minY, maxX, maxY);
    return graphicContext.getImageData(minX, minY, maxX-minX, maxY-minY);


    //CARE don't try getting pixels from canvas one by one
    //it is extremely slow (~8s per one circle)
    //i.e. dont' use getPixel()
};









//private ***********************************************************
Circle.prototype._distanceOfCentersSquared = function (circle){
    var dx = Math.abs(this.x - circle.x);
    var dy = Math.abs(this.y - circle.y);
    return dx*dx + dy*dy;
};













//debug ====================================================
/**
 * @returns {{x: number, y: number}}
 */
Circle.prototype.debug_getMBRTopLeft = function (){
    //CARE can return negative position
        //won't correct it, since it is a debug-only function
    var r= Circle.getBoostedRadius();
    return {
        x: this.x-r,
        y: this.y-r
    };
};
