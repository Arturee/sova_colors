//class
function Color(r,g,b,a){
    this.R=r;
    this.G=g;
    this.B=b;
    this.A=a;
}





//public methods *******************************************************
/**
 * @returns {HSLA}
 */
Color.prototype.toHSLA = function(){
    var r = this.R/255;
    var g = this.G/255;
    var b = this.B/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }
    return new HSLA(h,s,l,this.A/255);
    //http://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
};
Color.prototype.toHexString = function(){
    return '#' + Color._componentToTwoplaceHex(this.R)
                + Color._componentToTwoplaceHex(this.G)
                + Color._componentToTwoplaceHex(this.B);
    //make sure each component has two places!
    //(so that is doesnt represent a shorthand color by mistake!)
    //ignores alpha
};




Color.fromHex = function(hexString){
    //shorthand notation
    var result = /^#?([a-f\d])([a-f\d])([a-f\d])$/i.exec(hexString); //eg. "#03f" meaning #0033ff
    if (result != null){
        var rhex = result[1] + result[1];
        var ghex = result[2] + result[2];
        var bhex = result[3] + result[3];
    } else {
        //full notation
        result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexString); //eg. "#0033ff"
        if (result != null){
            rhex = result[1];
            ghex = result[2];
            bhex = result[3];
        } else {
            console.error("color format wrong: this should never happen");
            return null;
        }
    }
    var rgb = new Color(
        parseInt(rhex, 16),
        parseInt(ghex, 16),
        parseInt(bhex, 16),
        1   //CARE here we do not care about alpha
    );
    return rgb;
};
/**
 *
 * @param h e [0,1]
 * @param s e [0,1]
 * @param l e [0,1]
 * @returns {Color}
 */
Color.fromHSL = function(h, s, l){
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        };

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }
    return new Color(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255), 1);
    //http://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
};
Color.hslToHexString = function(h,s,l){
    return Color.fromHSL(h,s,l).toHexString();
};
/**
 * @param hue0 in [0,1]
 * @param hues
 * @param sats
 * @param lights
 * @returns {Array} of hexString
 */
Color.generatePalette = function (hue0, hues, sats, lights){
    var swatch = [];
    var hueNums = Color._generateHueNums(hues, hue0);
    var satNums = Color._generateSaturationNums(sats).reverse();
    var litNums = Color._generateShadeNums(lights).reverse();
    for (var h = 0; h <hues; h++){
        for (var l = 0; l<lights; l++){
            for (var s = 0; s<sats; s++){
                swatch.push(Color.hslToHexString(hueNums[h], satNums[s], litNums[l]));
            }
        }
    }
    var width = sats * lights;
    var greys = Color._generateGreyScale(width).reverse();
    return swatch.concat(greys);
    //console.log(hueNums);
    //console.log(satNums);
    //console.log(litNums);
};
/**
 *
 * @param h1
 * @param h2
 * @returns {number} in [0,1]
 */
Color.hueDistanceNormalized = function(h1, h2){
  return Color._hueDistance(h1, h2) * 2;
};







//private *******************************************************
Color._generateGreyScale = function (shades) {
    var swatch = [];
    var step = 255 / (shades - 1);
    //all shades but white
    for (var i = 0; i < shades - 1; i++) {
        var shadeValue = Math.round(step * i);
        var color = new Color(shadeValue, shadeValue, shadeValue, 1);
        swatch.push(color.toHexString());
    }
    swatch.push('#fff'); //white
    return swatch;
};
/**
 *
 * @param h1 in [0,1]
 * @param h2 in [0,1]
 * @returns {number} the shortest distance between two hues in [0, 0.5]
 * @private
 */
Color._hueDistance = function(h1, h2){
    var diff = Math.abs(h1 - h2);
    if (diff > 0.5){ //
        return 1 - diff;
    } else {
        return diff;
    }
    //hues form a cycle
    //in one way the distance is diff
    //in the other, it is 1-diff
    //we want the shorter/shortest distance
};
Color._generateHueNums = function(hues, hue0){
    if (hues<2){ return [hue0];}

    var nums=[];
    var step = 1/hues;
    for(var i=0;i<hues;i++){

        nums.push((hue0 + i*step) % 1);
    }
    return nums;
};
Color._generateShadeNums = function(shades){
    if (shades<2){ return [0.5];}

    var nums=[];
    const initial = 0.15;
    var step = (1-2*initial)/(shades-1);
    for(var i=0;i<shades;i++){
        nums.push(Math.min(initial + i*step, 1));
    }
    return nums;
};
Color._generateSaturationNums = function(sats){
    if (sats<2){ return [0.5];}

    var nums=[];
    const initial = 0.15;
    var step = (1-initial)/(sats-1);
    for(var i=0;i<sats;i++){
        nums.push(Math.min(initial + i*step, 1));
    }
    return nums;
};
/**
 * @param component RGB component in [0, 255]
 * @private
 */
Color._componentToTwoplaceHex = function(component){
    var comphex = component.toString(16);
    if (comphex.length === 1){
        comphex = '0' + comphex;
    }
    return comphex;
};