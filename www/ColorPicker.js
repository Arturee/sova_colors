/**
 * @param anchorElement HTML element, color picker will be its child
 * @param canvasController
 * @constructor
 */
function ColorPicker(anchorElement, canvasController){

    // CARE color picker element only exists when color picker is open
    // this class takes care of that and makes life much easier


    // CARE many hacks here
    // (more hacks also in Huebee sources ...)

    //TODO document a bit maybe
    var colorsPerRow = CONF.colorPickerSettings.saturations*CONF.colorPickerSettings.shades;
    var sats = CONF.colorPickerSettings.hues;
    var palette = Color.generatePalette(
        CONF.colorPickerSettings.hue0,
        CONF.colorPickerSettings.hues,
        CONF.colorPickerSettings.saturations,
        CONF.colorPickerSettings.shades
    );
    var huebeeSettings = {
        hues: colorsPerRow, //hack, makes sure the original palette is exactly the correct width
        shades: 1,          //hack makes sure the height will be correct
        staticOpen: false,  //open manually
        saturations: sats,  //hack makes sure the height will be correct
        customColors: palette
    };

    this.originalColor = CONF.circle.initialColor;
    this._isOpen = false;
    
    this.colorpicker = new Huebee(anchorElement, huebeeSettings);
    this.colorpicker.on('change', function (selectedColor, hue, sat, lum) {
        canvasController.changeColorOfFocusedCircle(selectedColor); //rgb hex string
        //TODO think about moving this out as callback
    });
    //hack: deals with 'unknown'

    /**
     * Huebee DOM structure
     *
     *
     * <.huebee>
     *     <.huebee__container>
     *         <.huebee__canvas>
     *         <.huebee__cursor>
     *         <.huebee__close-button>
     *
     */
    this.firstOpen = true;
    this.halfWidth = 0;

    //auto scroll down to see full color picker (when it leaves the screen downward)
    $(document).on('mouseenter', ColorPicker.selectorAll, function (event) {
        var pageHeight = $(document).height();
        window.scrollTo(0, pageHeight);
    });

    if (ColorPicker._used){
        throw new Error('singleton abused');
    } else {
        ColorPicker._used = true;
    }

}
/**
 *
 * @param cursorPositionInCanvasCss Position()
 * @param event
 */
ColorPicker.prototype.open = function(cursorPositionInCanvasCss, event){
    if (this.firstOpen){
        this.colorpicker.open();
        this._initDomDependantStuff();
        this.firstOpen = false;
    }

    if (!this.isOpen()){
        this._isOpen = true;
        this.colorpicker.open();
        this.move(cursorPositionInCanvasCss, event);
    } else {
        this.move(cursorPositionInCanvasCss, event);
    }
};
ColorPicker.prototype._initDomDependantStuff = function(){
    //add triggers to newly created colorpicker elemet
    var canvasElement = $(ColorPicker.selectorColorArea);

    var self=this;
    var canvasContainerElement = $(ColorPicker.selectorAll);
    canvasContainerElement.click(function(event){
        self.close();
        //CARE this doesnt remove canvasContainerElement ... event though it is removed from DOM!!!
        //very very very strange behariour by bhuebee
    });
    canvasContainerElement.contextmenu(function(event){
        self.setColor(self.originalColor);
        self.close();
        GuiController.preventDefaultEventHandling(event);
        //prevent little popup
    });
    this.halfWidth = canvasElement.width() / 2;
    //CARE canvasContainerElement.width() doesnt work //TODO why
};
ColorPicker.prototype.isOpen = function(){
    return this._isOpen;
};
ColorPicker.prototype.close = function(){
    this.colorpicker.close();
    this._isOpen = false;
};
/**
 *
 * @param cursorPositionInCanvasCss Position()
 * @param event
 */
ColorPicker.prototype.toggle = function(cursorPositionInCanvasCss, event){
    if (this.isOpen()){
        this.close();
    } else {
        this.open(cursorPositionInCanvasCss, event);
    }
};
/**
 * CARE only takes effect it colorpicker is open!
 * @param hexString
 */
ColorPicker.prototype.setColor = function(hexString){
    this.colorpicker.setColor(hexString);
    this.originalColor = hexString;
    //console.log(hexString);
};
/**
 * @param cursorPositionInCanvasCss Position()
 * @param event
 */
ColorPicker.prototype.move = function(cursorPositionInCanvasCss, event){
    if (!this.isOpen()){
        console.error('color picker moved when not visible');
        return;
    }

    var mouseX = event.clientX;
    var overflow = Math.max(0,(this.halfWidth - ($(window).width() - mouseX)));
    var offsetLeft = - this.halfWidth - overflow - 20;

    const OFFSET_DOWN = 80;
    var colorPickerContainer = $(ColorPicker.selectorAll);
    colorPickerContainer.css('left', cursorPositionInCanvasCss.x + offsetLeft);
    colorPickerContainer.css('top', cursorPositionInCanvasCss.y + OFFSET_DOWN);

    //console.log(cursorPositionInCanvasCss, offsetLeft, OFFSET_DOWN);
};



ColorPicker.selectorAll = '.huebee';
ColorPicker.selectorColorArea = '.huebee__canvas';



//private***********************************************************************
ColorPicker._used = false;