<?php
require_once 'Util.php';


/**
 *
 * sqlite is in-process (PHP process), serverless DB
 * usually ships as part (module) of PHP
 * uses simple files as backing store
 *
 * CARE many functions throw PDOException in case of error
 *
 *
 */
class Db {
    private $db = null;
    private static $instance = null;



    //* publics ****************************************************************
    public function getCountWorse($bindings){
        return $this->getCountHelper("<=", $bindings);
    }
    public function getCountBetter($bindings){
        return $this->getCountHelper(">", $bindings);
    }
    public function insert($bindings){
        $insert = "INSERT INTO data ("
            . Util::arrayToString(array_keys($bindings), ', ')
            .") VALUES ("
            . Util::arrayToString(array_keys($bindings), ', ', ':')
            . ")";
        $stmt = $this->db->prepare($insert);
        $stmt->execute($bindings);
        $stmt = null;

        //CARE date is inserted manually, because SQLlite may have trouble with time settings
        //(eg. like on XAMPP localhost)
    }
    public function getAllRows(){
        $stmt = $this->db->prepare("SELECT * FROM data");
        $stmt->execute();
        return $stmt->fetchAll();
    }





    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new Db();
        }
        return self::$instance;
    }

    //* privates ****************************************************************
    private function __construct() {
        $DB_FILENAME = 'db/colors.db';
        $mustCreateDB = !file_exists('./'.$DB_FILENAME);
        //CARE path has to begin with ./
        $this->db = new PDO('sqlite:' . $DB_FILENAME);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($mustCreateDB){
            $this->createTables();
        }
    }
    private function createTables(){
        $SQL_CREATE = <<<HEREDOC
            CREATE TABLE data (
                id INTEGER PRIMARY KEY,
                time_submitted TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                ip_address TEXT,
                client_id INTEGER,
                sex TEXT,
                age INTEGER,
                reload INTEGER,
                picture TEXT,
                resolution TEXT,
                score REAL,
                round_num INTEGER,
                colors TEXT
            )
HEREDOC;
        //heredoc end must be exactly at the beginning on line sadly
        //Id is auto-filled (may not be next integer, but will certainly be a larger one)
        $this->db->exec($SQL_CREATE);
        Util::err("db/colors.db and all tables were created.", false);
    }
    private function getCountHelper($comparatorString, $bindings){
        //TODO check type while binding?
        $stmt = $this->db->prepare(
            "SELECT COUNT(*) AS worse FROM data WHERE picture = :picture AND score "
            . $comparatorString . " :score"
        );
        $args = [
            'picture'=>$bindings['picture'],
            'score'=>$bindings['score']
        ];
        $stmt->execute($args);
        return $stmt->fetch()[0];
    }
}