//class
function Game(guiController, canvasController, canvasAnimator, pictureController){
    //TODO consider score/similarity class
    //TODO clean debug function

    this.canvasAnimator = canvasAnimator;
    this.guiController = guiController;
    this.canvasController = canvasController;
    this.pictureController = pictureController;

    this.round = 0;
    this.reloadNum = Game._getReload();
    this.roundSubmitted = false;
    this.reasonsWhyGameCantFinish = "Nothing happened yet";




    if (Game._used){
        throw new Error('singleton abused');
    } else {
        Game._used = true;
    }
}


//public methods ****************************************************************
Game.prototype.collectPlayerInfo = function(){
    this.guiController.showSlideInputInfo();
};
Game.prototype.start = function(){
    this.guiController.showSlideGame();
    this.nextRound();
};
Game.prototype.skipRound = function(){
    this.nextRound();
};
Game.prototype.dontSkipRound = function(){
    this.guiController.disableSkipping();
};
Game.prototype.canFinishRound = function(){
    if (this.canvasController.circles.length < CONF.circle.minCount){
        this.reasonsWhyGameCantFinish = 'You have to use at least '
            + CONF.circle.minCount
            + ' circles out of '
            + CONF.circle.maxCount;
        return false;
    } else {
        this.reasonsWhyGameCantFinish = null;
        return true;
    }
};
Game.prototype.finishRound = function(){
    if (this.roundSubmitted){ return; }

    this.canvasAnimator.disable();
    var score = this._computeScore();
    var resolution = this.pictureController.getSizePixels();
    var resolutionText = resolution.width + 'x' + resolution.height;
    if (DEBUG_MODE){
        resolutionText = 'debug mode ON';
    }
    var self = this;
    //AJAX POST method
    $.post(
        "backend.php",
        {
            picture: self.pictureController.getCurrentImageName(),
            colors: self.canvasController.circles.toString(),
            sex: self.guiController.getInfoSex(),
            age: self.guiController.getInfoAge(),
            client_id: Game._getID(),
            reload: self.reloadNum,
            score: Math.round(score * 100)/100,
            round_num: self.round,
            resolution: resolutionText
        },
        function callback(jsonBetterAndWorse){
            var others = JSON.parse(jsonBetterAndWorse);
            self.guiController.showScore(score, others.better, others.worse);
            if (self.nextRoundAvailable()){
                self.guiController.showButtonNextRound();
            } else {
                self.guiController.showEndOfGame();
            }

            if (self.round === 1){
                self.guiController.flashPicture();
            }
            console.log('data submitted to the server successfully ', new Date());
            //console.log(jsonBetterAndWorse);
        }
    );
    
    this.roundSubmitted = true;
};
Game.prototype.giveReasonsWhyGameCantFinish = function(){
    this.guiController.message(this.reasonsWhyGameCantFinish);
};
Game.prototype.nextRound = function(){
    this.roundSubmitted = false;
    var src = this.pictureController.showNextImage();
    if (src){
        var pixelSize = this.pictureController.getSizePixels();
        this.canvasController.setSize(pixelSize);
        this.canvasController.reset();
        this.canvasAnimator.disable();
        var self=this;
        this.guiController.showNewRound(src, function(){
            self.canvasAnimator.enable();
        });
        this.round++;
    } else {
        console.error('Next round not available');
    }
};
Game.prototype.nextRoundAvailable = function(){
    return this.pictureController.hasImage();
};

Game.prototype.fastForward = function(){
    if (this.guiController.countdownDuration > 2){ //TODO MEBBE less?
        this.guiController.skipCountdown();
    }
};








//private methods ********************************************************

/**
 * @returns {number} in [0,1]
 */
Game.prototype._computeScore = function(){
    var circles = this.canvasController.circles;
    if (circles.length === 0) {return 0;}
    //should never happen

    var virtualCanvas = this.pictureController.currentImageToVirtualCanvas();
    var context = virtualCanvas.getContext('2d');

    var sum = 0;
    circles.forEach(function(circle){
        var pixelData = circle.getMBRimageData(context, virtualCanvas.width, virtualCanvas.height);
        var colors = Game._imageDataToColors(pixelData);
        var similarity = Similarity.oneColorToMany(circle.getColor().toHSLA(), colors);
        sum += Math.min(similarity , 1);
    });
    var score = sum/circles.length;
    score = Math.min(score * CONF.score.overallScoreHelper, 1);
    return score;
};
/**
 * @param imageDataObject
 * @returns {Array} of HSLA()
 * @private
 */
Game._imageDataToColors = function(imageDataObject){
    //flat array of R1, G1, B1, A1, R2, G2, ....
    //each color starts at a multiple of 4
    var imageData = imageDataObject.data;
    var arr = [];
    var startOfColorIdx = 0;
    for (var rownum=0; rownum<imageDataObject.height; rownum++){
        for (var colnum=0; colnum<imageDataObject.width; colnum++){
            //going from 0
            var R = imageData[startOfColorIdx];
            var G = imageData[startOfColorIdx + 1];
            var B = imageData[startOfColorIdx + 2];
            var A = imageData[startOfColorIdx + 3];
            arr.push(new Color(R,G,B,A).toHSLA());
            startOfColorIdx += 4;
        }
    }
    return arr;
};
Game._getID = function(){
    const COOKIE_NAME = "id";
    var id = $.cookie(COOKIE_NAME);
    if (id == null){
        //if cookie is not set, id is undefined (ie. == null, but not === null)
        id=new Date().getTime();
        $.cookie(COOKIE_NAME, id); //sets cookie
    }
    return id;
};
Game._getReload = function(){
    const COOKIE_NAME = "reload";
    var reload = $.cookie(COOKIE_NAME);
    if (reload == null){
        reload = 1;
    } else {
        reload++;
    }
    $.cookie(COOKIE_NAME, reload); //sets cookie
    return reload;
};


Game._used = false;

































//debug methods ***************************************************************
Game.prototype.debug_showMBRs = function(reconvertColor){
    var circles = this.canvasController.circles;
    var virtualCanvas = document.createElement('canvas');
    //will never be shown in GUI (pixel data can only
    var img = this.pictureController.getCurrentImage();
    var size = this.pictureController.getSizePixels();
    virtualCanvas.width = size.width;
    virtualCanvas.height = size.height;
    var context = virtualCanvas.getContext('2d');
    context.drawImage(img, 0, 0);

    var self=this;
    var ctx = $('#gameCanvas')[0].getContext('2d');
    circles.forEach(function(c){
        var pixelData = c.getMBRimageData(context, size.width, size.height);
        var topLeft = c.debug_getMBRTopLeft();
        ctx.putImageData(pixelData, topLeft.x, topLeft.y);
    });
};
Game.prototype.debug_computeScore = function(){
    return this._computeScore();
};
Game.prototype.debug_showScore = function(score){
    this.guiController.showScore(score, 0, 0);
};














