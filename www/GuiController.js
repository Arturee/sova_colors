//class
function GuiController(){
    //TODO consider class score factory
    this.runningCountdown = null;
    this.countdownDuration = 0;
    this.countdownContainer = $('#countdown');

    this.buttonNext = $('input[name=next]');
    this.buttonAgain = $('input[name=again]');
    this.buttonSubmit = $('input[type=submit]');
    this.buttonSkip = $('input[name=skip]');
    this.buttonFaster = $('input[name=faster]');

    this.buttons=[
        this.buttonNext,
        this.buttonAgain,
        this.buttonSubmit,
        this.buttonSkip,
        this.buttonFaster
    ];

    this.messageInterval = null;
    this.owl = $('#owl');

    this.helpShowing = false;
    this.help = $('#helpContainer');
    this.helpPopup = $('#helpText');
    this.questionmark = $('#questionmark');

    this.picture = $('#picture');
    this.canvas = $('#gameCanvas');
    this.gameCanvasContainer = $('#gameCanvasContainer');

    this.starsContainer = $('#stars');
    this.scoreText = $('#scoreText');



    //init messages
    this.owl.tooltip({
        trigger: 'manual',
        html: true,
        animation: true
    });
    var self=this;
    this.owl.hover(function fnin(){
        self._showMessage();
    },
    function fnout(){
        self._hideMessageSlowly();
    });

    //init help
    this.questionmark.click(function(){
        if (self.helpShowing){
            GuiController._hide(self.helpPopup);
            self.questionmark.removeClass('clicked');
        } else {
            GuiController._show(self.helpPopup);
            self.questionmark.addClass('clicked');
        }
        self.helpShowing = !self.helpShowing;
    });





    if (GuiController._used){
        throw new Error('singleton abused');
    } else {
        GuiController._used = true;
    }
}





//public methods *******************************************************************
GuiController.prototype.showSlideInputInfo = function(){
    GuiController._hide($('#mainTutorial'));
    GuiController._show($('#mainGame'));
    this.message('Just input your info and start the game!');
};
GuiController.prototype.showSlideGame = function(){
    GuiController._hide($('#slideInputInfo'));
    GuiController._show(this.help);
    GuiController._show($('#slideGame'));
    //this.showButtonNextRound();
};
GuiController.prototype.showNewRound = function(src, callbackAfterCountdown){
    this._eraseCountdown();
    this._hideAllButtons();
    GuiController._show(this.buttonSkip);
    GuiController._show(this.buttonFaster);
    this._showNewPicture(src);
    this._setCanvasSize(this._getPictureSize());
    this._hideCanvas();
    const MESSAGE_NEXT_ROUND = "Focus on the picture. Memorize it!";
    this.message(MESSAGE_NEXT_ROUND);
    this._showTimerAndHelp();
    var self=this;
    this._fireCountdown([0, 1], [
        function(){
            callbackAfterCountdown();
            self._showCanvas();
            GuiController._enable(self.buttonSubmit);
            const MESSAGE_AFTER_COUNTDOWN = "Try to reproduce the colors and their" +
                "positions as accurately as possible.";
            self.message(MESSAGE_AFTER_COUNTDOWN);
        },
        function(){
            self._hidePicture();
            GuiController._hide(self.buttonFaster);
            GuiController._show(self.buttonSubmit);
            GuiController._disable(self.buttonSubmit);
        }
    ]);
};
GuiController.prototype.showEndOfGame = function(){
    this._eraseCountdown();
    this._hideAllButtons();
    GuiController._show(this.buttonAgain);
};
GuiController.prototype.disableSkipping = function(){
    GuiController._disable(this.buttonSkip);
    this.message("This is the last round. It cannot be skipped.");
};
GuiController.prototype.getInfoSex = function(){
    return $('#slideInputInfo input[name=sex]:checked').val();
};
GuiController.prototype.getInfoAge = function(){
    return $('input[name=age]').val();
};
GuiController.prototype.message = function(message){
    this.owl.attr('data-original-title', message);
    //CARE this is a hack, @title should be used normally instead of @data-original-title,
    // but it is bugged and does not work ...
    this._showMessage();
    this._hideMessageSlowly();
};
GuiController.prototype.showButtonNextRound = function(){
    this._hideAllButtons();
    GuiController._show(this.buttonNext);
};
GuiController.prototype.flashPicture = function(){
    GuiController._disable(this.buttonNext);
    GuiController._hide(this.scoreText);
    this._showPicture();
    var self = this;
    GuiController._doWithDelay(function(){
        $('#picture').addClass('seethrough');
    }, 3);
    GuiController._doWithDelay(function(){
        self._hidePicture();
        GuiController._show(self.scoreText);
        GuiController._enable(self.buttonNext);
    }, 4);
};
/**
 *
 * @param score in [0,1]
 * @param countBetter
 * @param countWorse
 */
GuiController.prototype.showScore = function(score, countBetter, countWorse){
    var starsHTML = GuiController._getStarsHTML(score);
    this.starsContainer.html(starsHTML);
    this.scoreText.html(GuiController._scoreText(score, countBetter, countWorse));
    GuiController._show(this.starsContainer);
    GuiController._show(this.scoreText);
    GuiController._hide(this.help);
    GuiController._hide(this.countdownContainer);
    this.starsContainer.addClass('fall-in');

    var congratulations = "";
    if (score > 0.95) {
        congratulations = "Congratulations! You've got the perfect score!";
    } else if (score > 0.7){
        congratulations = "Outstanding! How did you do that?";
    } else if (score > 0.5){
        congratulations = "Great memory! Keep it up!";
    } else if (score > 0.3) {
        congratulations = "Well done!"
    } else if (score > 0.1) {
        congratulations = "Good job!"
    } else {
        congratulations = "Tough luck, huh? Don't worry about it. Try another one!"
    }
    this.message(congratulations);
};
GuiController.prototype.skipCountdown = function(){ //TODO MEBBE connect with array of times ...
    const MIN_COUNT_DOWN = 2;
    this.countdownDuration = Math.min(this.countdownDuration, MIN_COUNT_DOWN);
    this.countdownContainer.html(this.countdownDuration);
};





GuiController.preventDefaultEventHandling = function(event){ //TODO maybe move
    if (event.preventDefault) {
        event.preventDefault();
    }
    else if (event.returnValue) { //legacy way (for older IE versions)
        event.returnValue = false;
    }
};
GuiController.eventOnScrollbar = function(e){ //TODO maybe move
    return e.clientX > $(window).width();
};















//private methods ********************************************************
GuiController.prototype._showNewPicture = function(src){
    this.picture.attr('src', src);
    //it's cleaner then appending Image() to DOM, and should be as fast
    this._showPicture();
};
GuiController.prototype._setCanvasSize = function(cssSize){
    this.canvas.css('width', cssSize.width);
    this.canvas.css('height', cssSize.height);
    //correct the position of canvas in GUI
    this.gameCanvasContainer.css('width', cssSize.width);
    this.gameCanvasContainer.css('height', cssSize.height);
};
/**
 *
 * @returns {{width: number, height: number}}
 */
GuiController.prototype._getPictureSize = function(){
    return {
        width: this.picture.width(),
        height: this.picture.height()
    };
    //console.log(this.picture);
};
/**
 *
 * @param times - array of times, when callbacks must be called (each time must be < CONF.countdownDuration!)
 * @param callbacks - array of functions to call at those times
 */
GuiController.prototype._fireCountdown = function(times, callbacks) {
    const COUNT_DOWN_STEP = 1000; //1s
    this.countdownDuration = CONF.countdownDuration;
    this.countdownContainer.html(this.countdownDuration);
    var self=this;
    this.runningCountdown = setInterval(function () {
        self.countdownDuration--;
        for(var i =0;i<times.length; i++){
            if (self.countdownDuration === times[i]){
                callbacks[i]();
            }
        }
        if (self.countdownDuration < 1){
            self._eraseCountdown();
        } else {
            self.countdownContainer.html(self.countdownDuration);
        }
    }, COUNT_DOWN_STEP);
};
GuiController.prototype._eraseCountdown = function(){
    if (this.runningCountdown != null){
        clearInterval(this.runningCountdown);
        this.countdownContainer.html("");
        this.runningCountdown = null;
    }
};
GuiController.prototype._showPicture = function(){
    GuiController._makeOpaque(this.picture);
    GuiController._show(this.picture);
};
GuiController.prototype._hidePicture = function(){
    GuiController._makeTransparent(this.picture);
    GuiController._hide(this.picture);
    GuiController._show($('#belowPicture'));
};
GuiController.prototype._showCanvas = function(){
    GuiController._makeOpaque(this.gameCanvasContainer);
};
GuiController.prototype._hideCanvas = function(){
    GuiController._makeTransparent(this.gameCanvasContainer);
};
GuiController.prototype._showTimerAndHelp = function(){
    GuiController._hide(this.starsContainer);
    GuiController._hide(this.scoreText);
    GuiController._show(this.help);
    GuiController._show(this.countdownContainer);
};
GuiController.prototype._hideAllButtons = function(){
    this.buttons.forEach(function(button){
        GuiController._hide(button);
    });
};
GuiController.prototype._showMessage = function(){
    clearInterval(this.messageInterval);
    //prevents hiding message due to a former "hide" event
    this.owl.tooltip('show');
};
GuiController.prototype._hideMessageSlowly = function(){
    GuiController._doWithDelay(function(){
        $('.tooltip').removeClass('in');
        /* this is a hack of bootstrap tooltip */
    }, CONF.owlShutUp);
};






/**
 *
 * @param score in [0,1]
 * @returns {string}
 * @private
 */
GuiController._getStarsHTML = function(score){
    var maxStars = CONF.score.stars;
    var achievedStars = maxStars*score;

    var integralPart = Math.floor(achievedStars);
    var decimalPart = achievedStars - Math.floor(achievedStars);

    var quarters = 0;
    if (decimalPart > 0.125 && decimalPart <= 0.375){
        quarters = 1;
    } else if (decimalPart > 0.375 && decimalPart <= 0.625){
        quarters = 2;
    } else if (decimalPart > 0.625 && decimalPart <= 0.875) {
        quarters = 3;
    } else if (decimalPart > 0.875) {
        //4 quarters
        integralPart++;
        quarters = 0;
    }
    var missing = maxStars - integralPart;
    if (quarters !== 0) { missing--; }

    const SRC_FULL = "imgs/star.png";
    const SRC_075 = "imgs/075star.png";
    const SRC_05 = "imgs/05star.png";
    const SRC_025 = "imgs/025star.png";
    const SRC_NONE = "imgs/nostar.png";

    var html = '';
    for (var i=0;i<integralPart;i++){
        html += GuiController._starImg(SRC_FULL);
    }
    switch(quarters){
        case 0: break;
        case 1: html += GuiController._starImg(SRC_025); break;
        case 2: html += GuiController._starImg(SRC_05); break;
        case 3: html += GuiController._starImg(SRC_075); break;
    }
    for (var i=0;i<missing;i++){
        html += GuiController._starImg(SRC_NONE);
    }
    //console.log(integralPart, quarters, missing, score);
    return html;
};
GuiController._starImg = function(src){
    const PREFIX = '<img src="';
    const SUFFIX = '" class="star">';
    return PREFIX + src + SUFFIX;
};
GuiController._scoreText = function(score, better, worse){
    var text = '<span class="scoreYour">Your score: ' + Math.round(score*100) + '%</span>';
    text += '<br><span class="scoreWorse">People worse than you: ' + worse + '</span>';
    text += '<br><span class="scoreBetter">People better than you: ' + better + '</span>';
    return text;
};
GuiController._hide = function(JElement){
    JElement.addClass('hidden');
};
GuiController._show = function(JElement){
    JElement.removeClass('hidden');
};
GuiController._isHidden = function(JElement){
    return JElement.hasClass('hidden');
};
GuiController._makeOpaque = function(JElement){
    JElement.removeClass('seethrough');
};
GuiController._makeTransparent = function(JElement){
    JElement.addClass('seethrough');
};
GuiController._disable = function(JElement){
    JElement.prop('disabled', true);
};
GuiController._enable = function(JElement){
    JElement.prop('disabled', false);
};
GuiController._doWithDelay = function(f, delay){ //TODO may be public
    var interval = setInterval(function () {
        f();
        clearInterval(interval);
    }, delay*1000);
};



GuiController._used = false;








/**
 GuiController._scoreText = function(score, better, worse){

    var text ='Your score: ' + Math.round(score*100) + '%';
    text += '<br><span class="others">';
    if (better === 1){
        text += '1 person was ';
    } else {
        text += better + ' people were ';
    }
    text += 'better than you and ' + worse + ' worse.</span>';
    return text;
};
*/