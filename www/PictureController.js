//class
/**
 * @param parentJElementOfPaths - jQuery element, whose children contain image paths as text
 * @constructor
 */
function PictureController(parentJElementOfPaths){
    this.imagePaths = PictureController._extractPathsFromHTML(parentJElementOfPaths);
    this.currentImage = null;
    this.preloadedImage = null;
    this.seen=new Array(60).fill(false);

    //assert
    if (this.imagePaths === null || this.imagePaths === 0){
        console.error('no images');
    }

    this._preloadOneImage();


    if (PictureController._used){
        throw new Error('singleton abused');
    } else {
        PictureController._used = true;
    }
}






//public methods ***************************************************************
/**
 * @returns {boolean} false if there isnt any src left to return
 */
PictureController.prototype.showNextImage = function(){
    this._getNextImage();
    if (this.currentImage!=null){
        return this._getCurrentImagePath();
    } else {
        return false;
    }
};
/**
 *
 * @returns {{width: Number, height: Number}} //TODO using size() class?
 */
PictureController.prototype.getSizePixels = function(){
    return {
        width: this.currentImage.width,
        height: this.currentImage.height
    };
};
PictureController.prototype.hasImage = function(){
    return this.preloadedImage != null;
};
PictureController.prototype.getCurrentImageName = function(){
    var path = this._getCurrentImagePath();
    var name = path.match(/[^\/]*$/)[0];
    return name;
    //console.log(name);
};
PictureController.prototype.getCurrentImage = function(){
    return this.currentImage;
};
PictureController.prototype.currentImageToVirtualCanvas = function(){
    var virtualCanvas = document.createElement('canvas');
    var size = this.getSizePixels();
    virtualCanvas.width = size.width;
    virtualCanvas.height = size.height;
    var context = virtualCanvas.getContext('2d');
    context.drawImage(this.currentImage, 0, 0);
    return virtualCanvas;
    //will never be shown in GUI
};






//private methods ***************************************************************
PictureController.prototype._getCurrentImagePath = function(){
    return this.currentImage.src;
    //setting this path as src of some <img> will not pass it this object
    //it will load the image again BUT this time very quickly from cache!
    //that's how all preloading works anywhere, so that will be fast enough
};
PictureController.prototype._getNextImage = function(){
    if (this.preloadedImage==null){
        this.currentImage = null;
    } else {
        this.currentImage = this.preloadedImage;
        this._preloadOneImage();
    }
};
/**
 *
 * @returns {*} null if all indexes have been used
 * @private
 */
PictureController.prototype._nextIndex = function(){
    var max = this.imagePaths.length;
    var luckyNumber = Math.floor(Math.random() * max);
    if (DEBUG_MODE){ luckyNumber = 0;}

    for (var i = luckyNumber; i<max; i++){
        if (!this.seen[i]){
            this.seen[i] = true;
            return i;
        }
    }
    for (var i = 0; i<luckyNumber; i++){
        if (!this.seen[i]){
            this.seen[i] = true;
            return i;
        }
    }
    return null; //everything was already seen
};
PictureController.prototype._preloadOneImage = function(){
    var idx = this._nextIndex();
    if (idx == null){
        this.preloadedImage = null;
    } else {
        this.preloadedImage = new Image();
        this.preloadedImage.src = this.imagePaths[idx];
    }
    //console.log('idx next', idx);
};






/**
 *
 * @param parentJElement - jQuery element
 * @returns {Array} of string
 * @private
 */
PictureController._extractPathsFromHTML = function(parentJElement){
    var paths = [];
    parentJElement.children().each(function(idx, child){
        paths.push(child.innerHTML);
    });
    return paths;
};


PictureController._used = false;