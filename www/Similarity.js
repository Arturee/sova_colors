/**
 * Created by Artur on 30-Mar-17.
 */
function Similarity(){
    console.error('static class instantiated');
}





// public methods ***********************************************************
/**
 * TODO unit test
 *
 * @param color HSLA()
 * @param pixelSquare array of array of HSLA()
 * @returns {number} in [0,1]
 */
Similarity.oneColorToMany = function(colorA, pixelSquare){
    var flatArray = [].concat.apply([], pixelSquare);
    var similarities = [];
    flatArray.forEach(function(colorB){
        similarities.push(Similarity._colors(colorA, colorB));
    });
    var topTen = Similarity._topTenPercent(similarities);
    var avg = Similarity._avg(topTen);
    return avg;
};







// private methods ***********************************************************
/**
 *
 * TODO unit test
 *
 * @param a HSLA()
 * @param b HSLA()
 * @returns {number} in [0, 1]
 * @private
 */
Similarity._colors = function(a,b){
    var dh = Color.hueDistanceNormalized(a.h, b.h);
    var ds = Math.abs(a.s - b.s);
    var dl = Math.abs(a.l - b.l);
    var euclideanDistanceSquared = dh*dh + ds*ds + dl*dl; //in [0,3]
    return Math.exp(-CONF.score.alpha*euclideanDistanceSquared);

    //CARE result is in [0.05, 1] for alpha = 1.
    //The lower boundary gets lower than 0,05 with alpha > 1
        //also decreasing resultant value
    //a conversely higher with alpha < 1
        //also increasing resultant value
};
//* TODO unit test
Similarity._topTenPercent = function(arr){
    var ninetyPercent = Math.floor(arr.length * 0.9);
    arr=arr.sort();
    arr=arr.splice(0, ninetyPercent);
    return arr;
};
//* TODO unit test
Similarity._avg = function(arr){
    var total = 0;
    arr.forEach(function(x){
        total +=x;
    });
    return total / arr.length;
};