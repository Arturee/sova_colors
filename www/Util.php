<?php

class Util {
    /**
     * @param $text
     * @param bool $exit
     */
    public static function err($text, $exit=true){
        $ERROR_LOG_FILE = 'log/error.txt';
        error_log(date('d.m.Y h:i:s') . ' ~ ' . $text . "\n", 3, $ERROR_LOG_FILE);
        //3 means log to file
        if ($exit){
            exit();
        }
    }
    /**
     * @param array $attrs
     * @return bool
     */
    public static function isRequestValid(array $attrs){
        foreach ($attrs as $attr){
            if (!isset($_POST[$attr])){
                return false;
            }
        }
        return true;
    }
    /**
     * Also adds time_submitted and ip_address
     * @param array $attrs
     * @return array bindings
     */
    public static function bindPostValuesToAttributes(array $attrs){
        $bindings=[];
        foreach ($attrs as $attr){
            $bindings[$attr] = $_POST[$attr];
        }
        $ip = $_SERVER['REMOTE_ADDR'];
        return array_merge(['time_submitted'=>date('d.m.Y h:i:s'), 'ip_address'=>$ip], $bindings);
    }
    /**
     * @param $array
     * @param string $separator
     * @param string $prefix
     * @param int $sizeof
     * @return string
     */
    public static function arrayToString($array, $separator, $prefix='', $sizeof=null){
        if (sizeof($array)===0){
            return '';
        }
        $str = $prefix . $array[0];
        if ($sizeof != null){
            $max = $sizeof;
        } else {
            $max = sizeof($array);
        }
        for ($i=1; $i<$max; $i++){
            $str .=  $separator . $prefix . $array[$i];
        }
        return $str;
    }
    /**
     * @param $rows
     * @param $header
     * @return string
     */
    public static function formatRows($rows, $header){
        $str = self::arrayToString($header, "\t") . "\n";
        foreach($rows as $row){
            $str .= self::arrayToString($row, "\t", '', sizeof($row)/2) . "\n";
        }
        return $str;
    }
}
