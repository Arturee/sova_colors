<?php
require_once 'Util.php';
require_once 'Db.php';


$attrs = [
        'client_id',
        'sex',
        'age',
        'reload',
        'picture',
        'resolution',
        'score',
        'round_num',
        'colors'
    ];



if (!Util::isRequestValid($attrs)){
    Util::err('(Bad request) ' . implode(';',array_keys($_POST)));
}


$bindings = Util::bindPostValuesToAttributes($attrs);
try {
    $db = Db::getInstance();
    echo json_encode([
        'better'=>$db->getCountBetter($bindings),
        'worse'=>$db->getCountWorse($bindings)
    ]);
    $db->insert($bindings);

} catch(PDOException $e){
    Util::err('(DB) ' . $e->getMessage());
}