<?php
require_once 'Util.php';
require_once 'Db.php';

header('Content-Type: text/plain');
//to show line breaks correctly

$headers = [
        'rowid',
        'timestamp',
        'IP address',
        'id',
        'sex',
        'age',
        'reload',
        'picture',
        'resolution',
        'score',
        'round',
        '[x,y,color,created(ms),last_edited(ms)],...'
    ];

try {
    $rows = Db::getInstance()->getAllRows();
    echo Util::formatRows($rows, $headers);

} catch(PDOException $e){
    err('(DB) ' . $e->getMessage());
}

