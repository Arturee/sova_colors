$( document ).ready(function() {
    //all JS code must be executed here - here we have a guarantee that the DOM is fully loaded
    //and all properties and elements are now correctly accessible
    'use strict';



    //TODO improve this init
    //game environment
    var picController = new PictureController($('#imagePaths'));
    var guiController = new GuiController();
    var canvasController = new CanvasController($('#gameCanvas'));
    var canvasAnimator = new CanvasAnimator(canvasController, guiController);

    var game = new Game(guiController, canvasController, canvasAnimator, picController);





    //game environment logic - set up event handlers
    $('#startSurvey').click(function(){
        game.collectPlayerInfo();
    });
    $('input[name=start]').click(function(){
        game.start();
    });
    $('input[name=next]').click(function(){
        game.nextRound();
    });
    $('input[name=again]').click(function(){
        location.reload();
    });
    $('input[name=skip]').click(function(){
        if (game.nextRoundAvailable()){
            game.skipRound()
        } else {
            game.dontSkipRound();
        }
    });
    $('input[name=faster]').click(function(){
        game.fastForward();
    });


    $('input[type=submit]').click(function(){
        if (game.canFinishRound()){
            game.finishRound();
        } else {
            game.giveReasonsWhyGameCantFinish();
        }
        return false;
        //When form submit button is clicked, first the click event is fired on the button
        //only if it returns true, then the submit event is fired on the form
    });




    



    //debug environment *********************************************************************
    if (DEBUG_MODE){
        $('input[name=debug_similarity]').click(function(){
            canvasController.enable();
            var score = game.debug_computeScore();
            game.debug_showScore(score);
        });
        $('input[name=debug_mbr]').click(function(){
            game.debug_showMBRs();
        });
    }
    $('form').submit(function(){
        console.error('submit event - this should never happen');
        return false;
    });



});