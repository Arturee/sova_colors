<?php
/*
 * CARE Make sure $dir has no subdirectories
 */
function getAllFilePaths($dir){
    $filenames = [];
    if ($handle = opendir($dir)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $filenames[] = $dir . '/' . $entry;
				//php doesn't mind which way the slash is tilted
            }
        }
        closedir($handle);
    }
    return $filenames;
}



$IMAGES_DIR = 'imgs/dataset';
$imagePaths = getAllFilePaths($IMAGES_DIR);
$debugmode = isset($_GET['debugmode']);
//CARE this is a trap door (add ?debugmode to url to enter debug mode)





?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!--
		CARE won't be seen by search engines (they need keywords and description (and a hint))
	-->
	<title>Color survey game</title>
	<!--styles-->
    <!--vendor-->
		<!-- development
			<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css">
		-->
		<!-- deployment -->
		<link rel="stylesheet" href="vendor/mini/bootstrap-3.3.7/css/bootstrap.min.css">
		<!-- both -->
		<link rel="stylesheet" href="vendor_hacked/huebee/huebee.css">
	<!--our-->
	<link rel="stylesheet" type="text/css" href="index.css">
</head>


<body>
<div class="hidden" id="imagePaths">
	<!-- list of all filenames of images - to be used by JS, not to be displayed -->
	<?php foreach ($imagePaths as $path){?>
	<div><?= $path ?></div>
	<?php } ?>
</div>













<header>
	<h1>Color survey game</h1>
</header>


<main id="mainTutorial" class="container-fluid">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<p class="grow">
				The picture below explains how the game/survey works. It's pretty easy.
				If you think you understand how it works, just go ahead and start playing!
			</p>
			<button id="startSurvey" type="button" class="btn btn-default">Let's play!
				<span class="arrow">&#10095;&#10095;</span>
			</button>
		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<img class="guideImage" src="imgs/tutorial1.png">
		</div>
	</div>
</main>






<main id="mainGame" class="container-fluid hidden">
<section class="row">
	<div class="col-md-4">
		<img id="owl" src="imgs/owl.png" data-toggle="tooltip" data-placement="bottom">
    </div>
	<div class="col-md-4">
		<h1 id="countdown"></h1>
		<div id="stars" class="hidden"></div>
	</div>
	<div class="col-md-4">
		<div id="helpContainer" class="hidden">
			<p>
				Memorize the picture. Then reproduce it's colors and their positions.
				<span id="questionmark" class="glyphicon glyphicon-question-sign" title="Click me to view help."></span>
			</p>
			<div id="helpText" class="hidden">
				(click the canvas to add a colorful circle)
				<br>(right-click to make a circle disappear)
				<br>(drag a circle to move it around)
				<br>
				<br>If something's not working, try opening this page on a larger screen
				(more than 1100px wide) and use Firefox, Chrome, Edge or Opera.
			</div>
		</div>
	</div>
</section>










<section id="slideInputInfo">
	<section class="row">
		<div class="col-md-5"></div>
		<div class="col-md-2">
			<form class="form-horizontal">
				<p>Please input your info... for science!</p>
				<div class="outlined">
					<div class="form-group">
						<label class="radio-inline">
							<input type="radio" name="sex" value="male" checked="checked"> Male
						</label>
						<label class="radio-inline">
							<input type="radio" name="sex" value="female"> Female
						</label>
					</div>
					<div class="form-group">
						Age
						<input type="number" name="age" value="21" class="form-control">
					</div>
					<input type="button" value="Start game!" name="start" class="btn btn-default">
					<!--
						CARE no need to save this in cookies. Because browsers save form information
						automatically, so that they can autocomplete forms
					-->
				</div>
			</form>
		</div>
		<div class="col-md-5"></div>
	</section>
</section>








<section id="slideGame" class="hidden">
	<section class="row">
		<div class="col-md-6">
			<img id="picture"><!--CARE image without src may cause trouble, in certain conditions -->
            <div id="scoreText" class="hidden"></div>
		</div>
		<div class="col-md-6">
			<div id="gameCanvasContainer" class="seethrough">
				<canvas id="gameCanvas"></canvas>
				<div id="colorpickerAnchor" class="hidden"></div>
                <!--
                    CARE canvas has to have a closing tag (same as <script>)
                    CARE canvas width and height must be set as attributes (not using CSS)
                        they are properties of internal canvas, not only the GUI representation
                        GUI representation's width and height can even be overridden with CSS
                    CARE width and height will be dynamically changed via JS
                -->
			</div>
		</div>
	</section>
	<section class="row">
		<div class="col-md-12">
			<form autocomplete="off">
                <!--CARE auotcomplete must be off due to a very very stupid bug my firefox
                    which autocompletes disabled buttons! Like wtf firefox.
                    30.3.2017
                 -->
                <input type="button" name="faster"  value="Faster"      class="btn btn-default hidden">
				<input type="submit"                value="Done"        class="btn btn-default">
				<input type="button" name="next"    value="Next round!" class="btn btn-default hidden">
				<input type="button" name="skip"    value="Skip"        class="btn btn-default">
				<input type="button" name="again"   value="Play again!" class="btn btn-default hidden">
                <!--
                    CARE a form without a submit button cannot be submitted via JS
                    (that is some kind of a weird rule in HTML)
                -->




				<?php if ($debugmode){ ?><!-- just for nice debug -->
					<input type="button" value="redo similarity" name="debug_similarity" class="btn btn-default">
					<input type="button" value="MBR" name="debug_mbr" class="btn btn-default">
					<p class="technicalHelp">redo similarity has to be clicked 2x to start working (first it just enables canvas)</p>
				<?php } ?>
			</form>
		</div>
	</section>
</section>

<footer>
	<p>
		<span id="thanks">Thank you for helping us understand how human color perception and memorization works.</span>
		<!--br><span class="unfinished">Site is under construction. (This is a beta version)</span-->
		<br><span>version 1.6.1</span>
	</p>




	<?php if ($debugmode){ ?><!-- just for nice debug -->
		<p>
			<a href="informative.txt">&lt; readmecko</a>
			<a href="CONF.js">CONF.js</a>
            <a href="info.php">php-info</a>
			<a href="data.php">collected data &gt;</a>
		</p>
	<?php } ?>
	<img id="logo" src="imgs/logo.png">
</footer>

</main>





<div><!--scripts------------------------------------------------------------------------->
	<!--vendor-->
        <!-- development
            <script src="vendor/jquery/jquery-3.1.1.js"></script>
            <script src="vendor_hacked/bootstrap/js/bootstrap.js"></script>
        -->
        <!-- deployment -->
        <script src="vendor/mini/jquery-3.1.1/jquery-3.1.1.min.js"></script>
        <script src="vendor/mini/bootstrap-3.3.7/js/bootstrap.min.js"></script>
		<script src="vendor_hacked/huebee/ev-emitter.js"></script>
		<script src="vendor_hacked/huebee/unipointer.js"></script>
		<script src="vendor_hacked/huebee/huebee.js"></script>
        <!--both-->
        <script src="vendor/jquery/jquery.cookie.js"></script>



	<!--our-->
        <script>
            <?php //just for nice debug
                if ($debugmode){
                    echo 'const DEBUG_MODE = true;';
                } else {
                    echo 'const DEBUG_MODE = false;';
                }
            ?>
        </script>
        <!--
            TODO make order-independent
            order:
                HSLA
                Color
                CONF
                [anything]
         -->
        <script src="HSLA.js"></script>
        <script src="Color.js"></script>
        <script src="CONF.js"></script>
        <script src="Position.js"></script>
        <script src="Size.js"></script>
        <script src="Circle.js"></script>
        <script src="CanvasController.js"></script>
        <script src="ColorPicker.js"></script>
        <script src="CanvasAnimator.js"></script>
        <script src="PictureController.js"></script>
        <script src="GuiController.js"></script>
        <script src="Similarity.js"></script>
        <script src="Game.js"></script>
        <script src="index.js"></script>
</div>

</body>
</html>
